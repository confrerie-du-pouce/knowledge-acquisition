#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, flash, redirect, url_for
from os import listdir
from os.path import isfile, join
from entityannotator import *
from werkzeug.utils import secure_filename

app = Flask(__name__)

UPLOAD_FOLDER = './datas/upload'
ALLOWED_EXTENSIONS = {'txt'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def homepage():
    return render_template("home.html")


@app.route('/about')
def about():
    return render_template("about.html")



@app.route('/parser', methods=['GET', 'POST'])
@app.route('/parser/<name>', methods=['GET', 'POST'])
def parser(name=''):
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filename_noext = filename.split('.')[0]
            file_content = open(os.path.join(app.config['UPLOAD_FOLDER'], filename), 'r', encoding="utf-8").read()
            return redirect(url_for('parser')+'/'+filename)
    if name == '':
        parsed = []
    else:
        path = os.path.join(app.config['UPLOAD_FOLDER'], name)
        file = open(path, 'r', encoding="utf-8")
        parsed = pos_tag(file.read())
    return render_template("parser.html", parsed=parsed)

@app.route('/entityviewer', methods=['GET', 'POST'])
@app.route('/entityviewer/<name>', methods=['GET', 'POST'])
def entityViewer(name=''):
    onlyfiles = [f for f in listdir('./datas/upload') if isfile(join('./datas/upload', f))]
    for i in range(len(onlyfiles)):
        filename, fileext = os.path.splitext(onlyfiles[i])
        onlyfiles[i] = filename
    if name == '':
        return render_template("entityviewerallfiles.html", files=onlyfiles)
    elif name not in onlyfiles:
        return render_template("entityviewerallfiles.html", files=onlyfiles, error='pas de fichier ' + name)
    else:
        predictlist = launchPredict(name)
        return render_template("entityviewer.html", list=predictlist, title=name)


if __name__ == '__main__':
    app.run(debug=True)