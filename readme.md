# Knowledge Acquisition

Il s'agit du projet de Knowledge Acquisition réalisé au second semestre de M1 IDC

## Fonctionnalité

- ajouté des textes à la base de donnée
- choisir un texte de la base de donnée
- affichage du texte ainsi que classement des mots par catégories choisi par l'algorithme de tri.

## module python à installer

- flask
- sklearn
- spacy
- unidecode

## lancer le serveur :

- dans le dossier, lancer la commande `python server.py` ou `python3 server.py`
- se rendre à l'adresse : http://localhost:5000
