from sklearn.feature_extraction import DictVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
import operator
import os
import json
import random
import pickle
import spacy
import unidecode

nlp = spacy.load("fr_core_news_sm")


def parser(fileName):
    """
    Function that parse the text used to create database
    :param fileName: name of the filname, string
    :return: list of list corresponding to the text lines, each word is a list too
    """
    data = open(fileName, "r", encoding="utf8")
    lignes = data.readlines()
    res = []
    for i in range(len(lignes)):
        m = []
        ligne = lignes[i]
        split_ligne = ligne.split()
        for j in range(len(split_ligne)):
            split = split_ligne[j].split("|")
            m.append(split)
        res.append(m)
    return res


def changePosTag(l):
    """
    Function that change text pos tags to spacy pos tags
    :param l: list of the parsed text
    :return: same list with pos tags changed
    """
    for i in l:
        for j in i:
            if 'PRO:PER' in j[1]:
                j[1] = 'PRON'
            if 'VER' in j[1]:
                j[1] = 'VERB'
            if 'DET' in j[1]:
                j[1] = 'DET'
            if 'PRP' in j[1]:
                j[1] = 'ADP'
            if 'KON' in j[1]:
                j[1] = 'CONJ'
            if 'INT' in j[1]:
                j[1] = 'INTJ'
            if 'NOM' in j[1]:
                j[1] = 'NOUN'
            if 'NAM' in j[1]:
                j[1] = 'PROPN'
            if 'PUN' in j[1]:
                j[1] = 'PUNCT'
    return l


def namedEntityClasses(neclist):
    """
    function that count occurrences of each entity tag
    :param neclist: list representing the parsed text
    :return: dictionnary of entity tag as key and their occurrences as value
    """
    nbword = 0
    for i in neclist:
        nbword += len(i)
    res = {}
    for j in neclist:
        for k in j:
            if k[2] != 'O':
                if k[2] in res:
                    res[k[2]] += 1
                else:
                    res[k[2]] = 1
    for l in res:
        res[l] = res[l] / nbword
    return res


def mostFrequentWord(wordlist):
    """
    function that save in a text file a list of the most frequent word in the text
    :param wordlist: list of the parsed text
    """
    allwords = {}
    res = []
    for i in wordlist:
        for j in i:
            if j[0] in allwords:
                allwords[j[0]] += 1
            else:
                allwords[j[0]] = 1
    sort = sorted(allwords.items(), key=operator.itemgetter(1), reverse=True)
    if len(sort) <= 200:
        for k in range(len(sort)):
            res.append(sort[k][0])
    else:
        for l in range(200):
            res.append(sort[l][0])
    f = open("mostFrequentWord.txt", "w+")
    for m in res:
        f.write(m + "\n")


def mostFrequentPOSTag(poslist):
    """
    function that save in a text file a list of the most frequent pos tags in the text
    :param poslist: list of the parsed text
    """
    allpos = {}
    res = []
    for i in poslist:
        for j in i:
            if j[1] in allpos:
                allpos[j[1]] += 1
            else:
                allpos[j[1]] = 1
    sort = sorted(allpos.items(), key=operator.itemgetter(1), reverse=True)
    if len(sort) <= 20:
        for k in range(len(sort)):
            res.append(sort[k][0])
    else:
        for l in range(20):
            res.append(sort[l][0])
    f = open("mostFrequentPOSTag.txt", "w+")
    for m in res:
        f.write(m + "\n")


def trainingData(linelist):
    """
    function that sort every word of the text by negatives ( "O" as entity tag) and positives (others) with their last
    and next word, and then put them in a json file
    :param linelist: list of the parsed text
    """
    wordlist = allLinesTogether(linelist)
    positives = []
    negatives = []
    for i in range(len(wordlist)):
        if wordlist[i][2] != "O":
            if i == 0:
                positives.append([['', ''], wordlist[i], wordlist[i + 1]])
            elif i == len(wordlist) - 1:
                positives.append([wordlist[i - 1], wordlist[i], ['', '']])
            else:
                positives.append([wordlist[i - 1], wordlist[i], wordlist[i + 1]])
        else:
            if i == 0:
                negatives.append([['', ''], wordlist[i], wordlist[i + 1]])
            elif i == len(wordlist) - 1:
                negatives.append([wordlist[i - 1], wordlist[i], ['', '']])
            else:
                negatives.append([wordlist[i - 1], wordlist[i], wordlist[i + 1]])
    pos = {"positives": positives}
    neg = {"negatives": negatives}
    with open('positives.json', 'w') as f:
        json.dump(pos, f)
    with open('negatives.json', 'w') as f2:
        json.dump(neg, f2)


def training(fileName, nb=101):
    """
    function that create a json file that contains a list of positives or negatives (depending of the filename) that can
    be used to train a classifier (because base file created with trainingData function is too big)
    :param fileName: name of the file
    :param nb: number of words to add to the json
    """
    toAdd = []
    with open(fileName + ".json") as json_file:
        data = json.load(json_file)
        tokens = data[fileName]
        nbTokens = len(tokens)
        for i in range(nb):
            ran = random.randint(0, nbTokens)
            toAdd.append(tokens[ran])
    dic = {fileName: toAdd}
    with open(fileName + str(nb) + ".json", 'w') as f:
        json.dump(dic, f)


def allLinesTogether(linelist):
    """
    function that put every words together in a list
    :param linelist: list of the parsed text
    :return: list of words
    """
    res = []
    for i in linelist:
        for j in i:
            res.append(j)
    return res


def createDictionnary(nb):
    """
    function that create list of dictionary for each words of the parsed text
    :param nb: int used to know which file to use (int corresponding of the number of words int the file)
    :return: list of dictionary
    """
    res = []
    tokens = getListOfAll(nb)
    for i in tokens:
        prevword = i[0][0]
        currentword = i[1][0]
        nextword = i[2][0]
        dict = {"last_token": prevword,
                "last_part-of-speech": i[0][1],
                "last_word-length": len(prevword),
                "last_is_capitalized": isCapitalize(prevword),
                "last_is_stopword": isStopword(prevword),
                "current_token": currentword,
                "current_part-of-speech": i[1][1],
                "current_word-length": len(currentword),
                "current_is_capitalized": isCapitalize(currentword),
                "current_is_stopword": isStopword(currentword),
                "next_token": nextword,
                "next_part-of-speech": i[2][1],
                "next_word-length": len(nextword),
                "next_is_capitalized": isCapitalize(nextword),
                "next_is_stopword": isStopword(nextword)}
        res.append(dict)
    return res


def isCapitalize(word):
    """
    function that test if the word is capitalize or not
    :param word: a word
    :return: 1 if the word is capitalize, 0 if not
    """
    if len(word) == 0:
        return 0
    if word[0].isupper():
        return 1
    return 0


def isStopword(word):
    """
    function that test if the word is a stop word or not
    :param word: a word
    :return: 1 if the word is a stop word, 0 if not
    """
    stopwordsfile = open("./datas/learningdatas/stopwords-fr.txt", "r")
    stopwords = stopwordsfile.read().split('\n')
    for i in stopwords:
        if word == i:
            return 1
    return 0


def vectorize(l):
    """
    function that vectorize a list of dictionary
    :param l: list of dictionary
    :return: the list vectorized
    """
    v = DictVectorizer(sparse=False)
    X = v.fit_transform(l)
    return X


def getListOfAll(nb):
    """
    function that create a list of all tokens, including positives and negatives
    :param nb: int used to know which file to use (int corresponding of the number of words int the file)
    :return: list of all tokens
    """
    tokens = []
    with open("positives" + str(nb) + ".json") as json_file:
        data = json.load(json_file)
        tokens = data["positives"]
    with open("negatives" + str(nb) + ".json") as json_file2:
        data2 = json.load(json_file2)
        for j in data2["negatives"]:
            tokens.append(j)
    return tokens


def createY(nb):
    """
    function that create Y, a list of all entity tags of the positives and negatives
    :param nb: int used to know which file to use (int corresponding of the number of words int the file)
    :return: list of all entity tags
    """
    tokens = getListOfAll(nb)
    res = []
    for i in tokens:
        res.append(i[1][2])
    return res


def createTrainTest(x, y):
    """
    function that create train and test variable who can be used to train and test a classifier
    :param x: X, a vector, created with vectorize function
    :param y: y, a list of entity tags, created with createY function
    :return: a list of train and test variables
    """
    x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.8, test_size=0.2)
    return [x_train, y_train, x_test, y_test]


def trainingClassifier(xtrain, ytrain):
    """
    function that create and train a randomForestClassifier
    :param xtrain: X variable used to train the classifier
    :param ytrain: y variable used to train the classifier
    :return: the trained classifier
    """
    clf = RandomForestClassifier(random_state=0)
    t = clf.fit(xtrain, ytrain)
    return t


def evaluateClassifier(xtest, ytest, classifier):
    """
    function that test the classifier
    :param xtest: X variable used to test the classifier
    :param ytest: y variable used to test the classifier
    :param classifier: the classifier to test
    :return: f1 score of the classifier
    """
    ypred = classifier.predict(xtest)
    pScore = f1_score(ytest, ypred, labels=list(set(ytest)), average='macro')
    return pScore


def pickleSave(c, name):
    """
    function that save a class with pickle
    :param c: the class to save
    :param name: the name of the file
    """
    pickle.dump(c, open("./datas/learningdatas/" + name + ".p", "wb"))


def pickleLoad(name):
    """
    function that load a class with pickle
    :param name: name of the file to load with pickle
    :return: class saved in the pickle file loaded
    """
    c = pickle.load(open("./datas/learningdatas/" + name + ".p", "rb"))
    return c


def pos_tag(text):
    """
    function that get pos tags of each words in a text with spacy
    :param text: a text
    :return: list of tokens and their pos tags
    """
    # text = remove_accents(text)
    doc = nlp(text)
    return [[token.text, token.pos_] for token in doc]


def cleanList(l):
    """
    funtcion that delete space from pos tags obtained via spacy pos tags
    :param l: list of tokens and their pos tags
    :return: same list, without space tokens
    """
    l2 = []
    for i in range(len(l)):
        if l[i][1] != 'SPACE':
            l2.append(l[i])
    return l2


def toText(text):
    """
    function that convert a list of strings into a string
    :param text: list of strings
    :return: a string
    """
    txt = ""
    for i in text:
        txt += i
    return txt


def createDictionnaryForNewText(l):
    """
    same function as createDictionnary, but this one is not used on the base file but on a text given
    :param l: list of the text
    :return: list of dictionary
    """
    res = []
    positives = []
    for i in range(len(l)):
        if i == 0:
            positives.append([['', ''], l[i], l[i + 1]])
        elif i == len(l) - 1:
            positives.append([l[i - 1], l[i], ['', '']])
        else:
            positives.append([l[i - 1], l[i], l[i + 1]])
    for j in positives:
        prevword = j[0][0]
        currentword = j[1][0]
        nextword = j[2][0]
        dict = {"last_token": prevword,
                "last_part-of-speech": j[0][1],
                "last_word-length": len(prevword),
                "last_is_capitalized": isCapitalize(prevword),
                "last_is_stopword": isStopword(prevword),
                "current_token": currentword,
                "current_part-of-speech": j[1][1],
                "current_word-length": len(currentword),
                "current_is_capitalized": isCapitalize(currentword),
                "current_is_stopword": isStopword(currentword),
                "next_token": nextword,
                "next_part-of-speech": j[2][1],
                "next_word-length": len(nextword),
                "next_is_capitalized": isCapitalize(nextword),
                "next_is_stopword": isStopword(nextword)}
        res.append(dict)
    return res


def predictWordsTag(l, c):
    """
    function used to predict the entity tags of a list of dictionaries
    :param l: list of dictionaries
    :param c: classifier
    :return: list of prediction for the list given
    """
    x = pickleLoad("vectorizer2")
    x2 = x.transform(l)
    return c.predict(x2)


def prediction(txt):
    """
    function that gives tokens and their predictions from the classifier
    :param txt: text to predict
    :return: list of tokens and their predictions
    """
    res = []
    c = pickleLoad("random10000")
    txt = toText(txt)
    pos = pos_tag(txt)
    l2 = cleanList(pos)
    d = createDictionnaryForNewText(l2)
    predict = predictWordsTag(d, c)
    for i in range(len(l2)):
        res.append([l2[i], predict[i]])
    return res


def remove_accents(input_str):
    """
    function that removes accents from a text, unused
    :param input_str: text
    :return: text without accent
    """
    noacc = unidecode.unidecode(input_str)
    return noacc


def launchPredict(filename):
    """
    function that do a prediction on a file, delete occurrences and sort by entity tags, excluding "O" tag
    :param filename: name of the file to be tested
    :return: sorted list of entity tags
    """
    txt = open("./datas/upload/" + filename + ".txt", encoding="utf-8").readlines()
    p = prediction(txt)
    res = []
    for i in p:
        if i[1] != "O":
            if i[0][1] != "DET":
                res.append(i)
    res = deleteOccurences(res)
    res = trier(res)
    res.append(toText(txt))
    return res


def deleteOccurences(list):
    """
    function that let only one occurrences of each words in a list of prediction
    :param list: list of prediction
    :return: clean list with one occurrences for each words
    """
    tested = ["de", "d'"]
    res = []
    for i in list:
        if i[0][0] not in tested:
            res.append(i)
            tested.append(i[0][0])
    return res


def trier(list):
    """
    function that sort a list of prediction by entity tags
    :param list: list of prediction
    :return: sorted list, containing 4 list, one for each entity tags
    """
    per = []
    loc = []
    misc = []
    org = []
    for i in list:
        if i[1] == 'I-PER':
            per.append(i)
        elif i[1] == 'I-LOC':
            loc.append(i)
        elif i[1] == 'I-MISC':
            misc.append(i)
        else:
            org.append(i)
    return [per, loc, org, misc]
