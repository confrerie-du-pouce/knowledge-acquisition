
                Y figurent cinq nouveaux romans, quelques anciens prestigieux, dont Daniel Picouly, et le chouchou de L'Express, Nicolas Mathieu.
                
                    C'est à Nancy, au festival du Livre sur la place, que l'Académie Goncourt, qui en est l'une des vedettes cette année, a fait circuler sa "première sélection". Dans cette liste de quinze romans, sauf surprise et nouveaux candidats dans les suivantes, devrait figurer le prochain lauréat, dont le nom sera rendu public Chez Drouant le 7 novembre prochain.
                
                
                    Parmi les absents, quelques poids lourds, dont Maylis de Kerangal, Serge Joncour ou Philippe Lançon, dont Le Lambeau a pourtant fait l'unanimité au printemps. D'autres sont bien présents: Eric Fottorino, Thomas B. Reverdy, Tobie Nathan, François Vallejo ou le très populaire Daniel Picouly.
                
                
                    Mais les jurés ont surtout mis l'accent sur la jeunesse et les premiers romans, ceux d'Inès Bayard, Meryem Alaoui, Pauline Delabroy-Allard, David Diop ou de la Belge Adeline Dieudonné: cinq sur quinze, c'est plus qu'un signal.
                
                
                    Ces autres chiffres: trois journalistes (Eric Fottorino, Gilles Martin-Chauffier et Clara Dupont-Monod); trois Gallimard, deux Albin Michel, deux Grasset et deux Stock, qui relativisent à peine la domination habituelle du trio GalliGraSeuil.
                
                
                    La liste complète:
                    Meryem Alaoui, La Vérité sort de la bouche du cheval (Gallimard)
                
                
                    Inès Bayard, Le Malheur du bas (Albin Michel)
                
                
                    Guy Boley, Quand Dieu boxait en amateur (Grasset)
                
                
                    Pauline Delabroy-Allard, Ca raconte Sarah (Editions de Minuit)
                
                
                    Adeline Dieudonné, La Vraie vie (L'Iconoclaste)
                
                
                    David Diop, Frère d'âme (Seuil)
                
                
                    Clara Dupont-Monod, La Révolte (Stock)
                
                
                    Eric Fottorino, Dix-sept ans (Gallimard)
                
                
                    Paul Greveillac, Maîtres et Esclaves (Gallimard)
                
                
                    Nicolas Mathieu, Leurs Enfants après eux (Actes Sud)
                
                
                    Gilles Martin-Chauffier, L'Ere des suspects (Grasset)
                
                
                    Tobie Nathan, L'Evangile selon Youri (Stock)
                
                
                    Daniel Picouly, Quatre-vingt-dix secondes (Albin Michel)
                
                
                    Thomas B. Reverdy, L'Hiver du mécontentement (Flammarion)
                
                
                    François Vallejo, Hôtel Waldheim (Viviane Hamy)
                
            