
                Le chef du RN niçois a déposé un recours pour "excès de pouvoir" et dénonce une violation de la loi de 1905 sur la laïcité.
                
                    L'affaire revient devant la justice. Philippe Vardon, responsable niçois du Rassemblement national (RN), avait déjà saisi le procureur de la République en juin dernier pour dénoncer des prières de rue pendant l'Aïd-el-Fitr à Nice. Des centaines de musulmans avaient prié sur le parking du théâtre Lino-Ventura, en plein air, déclenchant la colère de certains habitants et élus, dont le maire de la ville Christian Estrosi (Les Républicains).
                
                
                    Cette fois, c'est contre la mairie de Nice que Philippe Vardon compte se battre en justice, il a déposé un recours pour "excès de pouvoir". Il reproche à la municipalité d'avoir autorisé gratuitement des musulmans à occuper le théâtre, ce qui contrevient, selon lui et son avocat, à la loi sur la laïcité de 1905. La gratuité "pour ce type d'événement, à caractère cultuel, c'est interdit", déclare Me Pierre-Vincent Lambert, avocat de l'élu RN, dans Nice Matin.
                
                
                    La loi sur la laïcité de 1905 n'interdit pas exactement la gratuité des prêts, mais précise à l'article 19 que les associations cultuelles "ne pourront, sous quelque forme que ce soit, recevoir des subventions de l'État, des départements et des communes".
                
                
                    Le scandale de l'Aïd-el-Fitr au théâtre
                    Le 15 juin dernier à Nice, un millier de musulman étaient allés fêter l'Aïd-el-Fitr, marquant la fin du jeûne du Ramadan, au théâtre Lino-Ventura. C'est au départ pour éviter les prières de rue que la salle est fournie à l'Union des musulmans des Alpes-Maritimes (Umam), par la mairie, car elle ne trouve pas d'endroits où réunir ses fidèles. Mais la célébration est victime de son succès, et les femmes se réunissant à l'intérieur, beaucoup d'hommes venus prier se retrouvent dehors: "Nous avons donc contenu un nombre maximum de personnes sur le parking pour éviter un débordement directement dans la rue", expliquait alors le recteur et responsable de l'Umam à 20 Minutes.
                
                
                    Ces prières sur le parking d'un bâtiment de la ville avaient fait scandale, des dizaines de plaintes avaient été déposées, notamment concernant le bruit des prières. "Je condamne avec la plus grande fermeté les prières de rue qui sont organisées à l'Ariane depuis 6h ce matin. Les lois de la République doivent être respectées partout" avait immédiatement déclaré Christian Estrosi.
                
                
                    L'avocat de Philippe Vardon a retrouvé l'arrêté municipal qui autorisait le prêt gratuit de ce bâtiment public à l'Union des musulmans des Alpes-Maritimes. S'il y est noté le bénéficiaire du prêt, l'objet cultuel n'est pas précisé. "C'est la première fois que je vois ça: vous êtes autorisés à occuper l'espace public, mais on ne sait pas pourquoi", déclare-t-il dans Nice Matin, "je pense que ce n'est pas une erreur mais que c'est volontaire".
                
                
                    La mairie répond de son côté que "la Ville de Nice considère qu'il lui appartient de permettre l'expression des différents cultes notamment lors des fêtes traditionnelles." En juin, la municipalité s'était déjà défendue après les attaques, assurant que si elle avait bien prêté la salle, elle n'avait pas autorisé l'utilisation du parking, ni un volume sonore considéré comme fort par les riverains.
                
                
                    Une précédente condamnation en 2013
                    Philippe Vardon suggère que son opposant politique Christian Estrosi entretient les organisations musulmanes de la ville afin d'obtenir leur vote par la suite. Dans le quotidien régional, il rappelle "le précédent de la mosquée de la rue de Suisse", le RN "avait fait condamner la Ville [de Nice] en 2013 pour le loyer sous-évalué de la mosquée installée dans des locaux municipaux".
                
            